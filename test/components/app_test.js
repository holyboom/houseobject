import { renderComponent, expect } from '../test_helper';
import App from '../../src/components/app';

// Use 'describe' to group together similar tests
describe('App', () => {
  let component;

  beforeEach(() => {
    component = renderComponent(App);
  });

  it('shows a input box', () => {
    expect(component.find('.input-box')).to.exist;
  });

  it('shows a output box', () => {
    expect(component.find('.output-box')).to.exist;
  });
});
