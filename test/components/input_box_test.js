import { renderComponent, expect } from '../test_helper';
import InputBox from '../../src/components/input_box';

describe('InputBox', () => {
  let component;

  beforeEach(() => {
    component = renderComponent(InputBox);
  });

  it('has the correct class', () => {
    expect(component).to.have.class('input-box');
  });

  it('has a text area', () => {
    expect(component.find('textarea')).to.exist;
  });

  it('has a button', () => {
    expect(component.find('button')).to.exist;
  });

  describe('entering some text', () => {
    beforeEach(() => {
      component.find('textarea').simulate('change', 'new comment');
    });

    it('shows that text in the textarea', () => {
      expect(component.find('textarea')).to.have.value('new comment');
    });

  });
});
