import { renderComponent, expect } from '../test_helper';
import OutputBox from '../../src/components/output_box';
import sampleTest from '../../sampleTest';
import sampleResult from '../../sampleResult';

describe('OutputBox', () => {
  let component;

  beforeEach(() => {
    const props = {inputReducer: sampleTest};
    component = renderComponent(OutputBox, null, props);
  });

  it('shows correct result that is provided', () => {
    expect(component).to.contain(JSON.stringify(sampleResult));

  });

  it('shows error result that is provided', () => {
    expect(component).to.have.class('error-content');

  });
});
