import { expect } from '../test_helper';
import { SAVE_INPUT } from '../../src/actions/types';
import { saveInput } from '../../src/actions';
import sampleTest from '../../sampleTest';

console.log(sampleTest);

describe('actions', () => {
  describe('saveInput', () => {
    it('has the correct type', () => {
      const action = saveInput();
      expect(action.type).to.equal(SAVE_INPUT);
    });

    it('has the correct payload', () => {
      const action = saveInput(sampleTest);
      expect(action.payload).to.equal(sampleTest);
    });
  });
});
