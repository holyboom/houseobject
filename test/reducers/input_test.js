import { expect } from '../test_helper';
import inputReducer from '../../src/reducers/reducer_input';
import { SAVE_INPUT } from '../../src/actions/types';
import sampleTest from '../../sampleTest';

describe('INPUT Reducer', () => {
  it('handles action with unknown type', () => {
    expect(inputReducer(undefined, {})).to.eql({});
  });

  it('handles action of type SAVE_INPUT', () => {
    const action = { type: SAVE_INPUT, payload: sampleTest };
    expect(inputReducer({}, action)).to.eql(sampleTest);
  });
});
