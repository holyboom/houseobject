# House Object


###Getting Started###

Step 1

The first step focuses only on the front-end side.

Create a JavaScript application using React (and any other library/tool you want/need).
The application must have an editable field where the user can paste a formatted JSON (Input).
The application must show a non-editable field displaying the updated JSON (Output).
The application must have automated tests.
Here's an example of the UI (feel free to ignore it and do your own if you can do something better, this is just to give you a better idea of what's expected).
(Bonus) Show off your CSS skills by making it look good.

###Tools###
React, Redux, Webpack, Mocha, Chai

###Getting Started###

There are two methods for getting started with this repo.

####Familiar with Git?#####
Checkout this repo, install depdencies, then start the gulp process with the following:

```
	> git clone holyboom@bitbucket.org:holyboom/houseobject.git
	> cd houseobject
	> npm install
	> npm start
```

####Not Familiar with Git?#####
Click [here](https://bitbucket.org/holyboom/houseobject) then download the .zip file.  Extract the contents of the zip file, then open your terminal, change to the project directory, and:

```
	> npm install
	> npm start
```


