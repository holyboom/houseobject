import React from 'react';
import { Component } from 'react';
import InputBox from './input_box';
import OutputBox from './output_box';

export default class App extends Component {
  render() {
    return (
      <div className="container main-content">
        <div className="row">
          <InputBox />
          <OutputBox />
        </div>
      </div>
    );
  }
}
