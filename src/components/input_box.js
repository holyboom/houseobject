import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

class InputBox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      input: ''
    };
  }

  componentDidMount(){
    this.textareaInput.focus();
  }

  handleChange(event) {
    this.setState({ input: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();

    this.props.saveInput(this.state.input);
  }

  onKeyDown(e){
    if (e.keyCode === 9) {
      e.preventDefault();
    }
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit.bind(this)} className="input-box col">
        <h5>INPUT</h5>
        <textarea
          ref={(input) => { this.textareaInput = input; }}
          value={this.state.input}
          onChange={this.handleChange.bind(this)}
          onKeyDown={this.onKeyDown}
        />
        <div>
          <button className="btn btn-primary" action="submit">Submit</button>
        </div>
      </form>
    );
  }
}

export default connect(null, actions)(InputBox);
