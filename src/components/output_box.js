import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from '../actions';

class OutputBox extends Component {

  constructor(){
    super();
    this.state = {
      error: '',
      inputstring: '',
    };
  }


  componentWillReceiveProps(nextProps) {
    if(this.props.input !== nextProps.input ) {
      let error = '';
      let inputstring = '';

      try {
        inputstring = JSON.parse(nextProps.input);
      } catch(e) {
        error = 'invalid syntax'
      }
      if(nextProps.input === '') {
        error = 'empty input'
      }

      if(!error) {
        let level = Object.keys(inputstring).length-1;
        try {
          this.groupObjectrefac(level, inputstring);
        } catch(e) {
          error = e.message;
        }
        this.setState({inputstring, error});

      } else {
        this.setState({error});
      }
    }
  }

  checkInvalidID(idkeyChild, parentkeyChild) {
    if(idkeyChild === parentkeyChild) throw {message: "ID key and parent key cannot be the same"};
  }

  checkInvalidLevel(childLevel, parentLevel ) {
    if(childLevel <= parentLevel) throw {message: "childLevel cannot less than or equal parentLevel"};
  }

  groupObjectrefac(level, inputstring) {

    if(level === 0){
      return inputstring
    }

    let ObjectChild = inputstring[level];
    let ObjectMom = inputstring[level-1];

    if(inputstring[0][0].parent_id != null){
      throw {message: "input string level 0,parent_id have to be null"};
    }

    for(let i=0; i< ObjectChild.length; i++){
      let idkeyChild = ObjectChild[i].id;
      let parentkeyChild = ObjectChild[i].parent_id;
      this.checkInvalidID(idkeyChild,parentkeyChild)
      let levelChild = ObjectChild[i].level;
      let found = false;

      for(let j=0; j< ObjectMom.length; j++){
        this.checkInvalidLevel(levelChild, ObjectMom[j].level)
        if(ObjectMom[j].id === parentkeyChild){
            ObjectMom[j].children.push(ObjectChild[i])
            found = true
        }
      }

      if(!found) {
        throw {message: "childID not in level - 1"};
      }
    }
    level --;
    this.groupObjectrefac(level,inputstring)
  }

  render() {
    return (
      <div className="col output-box">
        <h5>OUTPUT</h5>
        <div className="output-content">
          {this.state.error ?
            <div className="error-content">{this.state.error}</div>
            :
            <pre>{JSON.stringify(this.state.inputstring[0], null, 2)}</pre>
          }
        </div>
      </div>
    );
  }
};

function mapStateToProps(state) {
  return { input: state.inputReducer };
}

export default connect(mapStateToProps, actions)(OutputBox);
