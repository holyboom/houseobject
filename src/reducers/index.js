import { combineReducers } from 'redux';
import inputReducer from './reducer_input';

const rootReducer = combineReducers({
  inputReducer: inputReducer
});

export default rootReducer;
