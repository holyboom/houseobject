import { SAVE_INPUT } from '../actions/types';

export default function(state = {}, action) {
  switch(action.type) {
    case SAVE_INPUT: {
      return action.payload ;
    }
  }

  return state;
}
