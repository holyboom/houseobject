import { SAVE_INPUT } from './types';

export function saveInput(input) {
  return {
    type: SAVE_INPUT,
    payload: input
  };
}
